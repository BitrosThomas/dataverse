package com.thomas.dataverse.service;

import com.thomas.dataverse.domain.User;

import java.util.List;

public interface UserService {

    public List<User> findAll();

    public void save(User theUser);

    User findUserByEmail(String email);

    public User findById(Long theId);

}

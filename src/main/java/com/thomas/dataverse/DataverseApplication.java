package com.thomas.dataverse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataverseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataverseApplication.class, args);
	}

}


package com.thomas.dataverse.repository;

import com.thomas.dataverse.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    public List<User> findAll();
    User findByEmail(String email);



}
